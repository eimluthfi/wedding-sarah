# Walimah Website


Web Portal for online invitation. Powered by Love :two_hearts:. 

The project currently supports:
  - Date countdown.
  - Customize greeting for guest.
  - Telegram bot support for inform guest that confirmed to attend.

## Tech

Application is using following libraries:

* Flask - is a micro web framework written in Python.
* Waitress - is a production-quality pure-Python WSGI server.
* SQLite - is a relational database management system contained in a C library.
* Python-Telegram-Bot - is a pure Python interface for the Telegram Bot API.
* Python-dotenv - reads key-value pairs from a .env file and can set them as environment variables.

## Installation
Make sure you already install [Python](https://www.python.org/downloads/ "Download Python") in your machine(linux usually already including python in the OS)

Once cloned on your local machine. Use to following commands to run the application.
```sh
$ cd wedding-sarah
$ pip install -r requirements.txt
```
## Start up the App

After all these steps , run the WSGI server.

```sh
$ python wedding/server.py
```
you can now access portal using this link http://localhost

## Docker

Use this to build the image
```sh
$  docker-compose build
```

### Run using docker
Initiate database before we can run the docker container. You may want to create persistent sqlite DB. So you can mount it using this when run the container.
```sh
$  touch wedding/database/wedding_db.sqlite
```

make sure to modified .env file to suit your configurations

run the container
```sh
$  docker-compose up
```