#!/usr/bin/env python3
import os
import sys
import sqlite3
import telegram
from flask import Flask, jsonify, abort, request, make_response, url_for, render_template, redirect, flash, send_from_directory, send_file
from waitress import serve
from datetime import datetime
from dotenv import load_dotenv
load_dotenv()

HOST: str = os.getenv('HOST', '0.0.0.0')
PORT: int = os.getenv('PORT', 80)
URL_SCHEME: str = os.getenv('URL_SCHEME', 'http')
TELEGRAM_BOT: str = os.getenv('TELEGRAM_BOT', 'OFF')
TELEGRAM_BOT_TOKEN: str = os.getenv('TELEGRAM_BOT_TOKEN')

app = Flask(__name__, static_url_path='/static')

@app.before_request
def clear_jinja_cache():
    # When you import jinja2 macros, they get cached which is annoying for local
    # development, so wipe the cache every request.
    if 'localhost' in request.host_url or '0.0.0.0' in request.host_url:
        app.jinja_env.cache = {}

app.before_request(clear_jinja_cache)

conn = sqlite3.connect(os.path.join(app.root_path, "wedding_db.sqlite"), check_same_thread=False)
conn.execute("PRAGMA foreign_keys = 1")
conn.row_factory = sqlite3.Row
cursor = conn.cursor()

def init_db():
    cursor.execute('''CREATE TABLE IF NOT EXISTS guest_log_table
    (id INTEGER PRIMARY KEY,
    name    TEXT    NOT NULL,
    email   TEXT     NOT NULL,
    created    TIMESTAMP   NOT NULL)''')

    cursor.execute('''CREATE TABLE IF NOT EXISTS telegram_table
    (id INTEGER PRIMARY KEY,
    first_name    TEXT    NOT NULL,
    last_name   TEXT     NOT NULL,
    created    TIMESTAMP   NOT NULL)''')

    conn.commit()

def validate_telegram():
    global telebot, TELEGRAM_BOT
    if TELEGRAM_BOT=='OFF':
        return
    try :
        telebot = telegram.Bot(token=TELEGRAM_BOT_TOKEN)
        print("Telegram Bot is activated. Ready to send messages.")
    except telegram.error.InvalidToken:
        TELEGRAM_BOT='OFF'
        return

def update_tele_user():
    # Check if someone already chat the bot, so they automatically will get the message if guest confirm
    try:
        updates = telebot.get_updates()
    except telegram.error.TimedOut:
        print("Bot cannot found new message")
        return
    for update in updates:
        user_id = update.message.from_user.id
        first_name = update.message.from_user.first_name
        last_name = update.message.from_user.last_name
        cursor.execute('SELECT id FROM telegram_table WHERE id=? ', (user_id,))
        entry = cursor.fetchone()
        if entry:
            continue
        else:
            print("Add new Telegram User {} {}".format(first_name, last_name))
            cursor.execute('INSERT INTO telegram_table (id, first_name, last_name, created) VALUES (?,?,?,?)', (user_id, first_name, last_name, datetime.today().strftime('%H:%M %d %B %Y')))
            conn.commit()

def bot_send(name : str, email : str):
    if TELEGRAM_BOT=='OFF':
        return
    update_tele_user()
    cursor.execute('SELECT id FROM telegram_table')
    entry = cursor.fetchall()

    for user in entry:
        telebot.send_message(text='InsyaAllah {} hadir.\nBoleh dikontak via {}'.format(name, email), chat_id=user['id'])

@app.route('/')
def index():
    guest_name = request.args.get('to', type = str)
    return render_template('index.html', guest=guest_name)

@app.route('/confirmation', methods=['POST'])
def guest_confirm():
    name = request.form.get('name', type = str)
    email = request.form.get('email', type = str)
    print("got new confirmation from",name, email)

    cursor.execute('INSERT INTO guest_log_table (name, email, created) VALUES (?,?,?)', (name, email, datetime.today().strftime('%H:%M %d %B %Y')))
    conn.commit()

    bot_send(name, email)
    return make_response("OK", 200)

if __name__ == '__main__':
    init_db()
    validate_telegram()
    print("\nRunning on {}:{}...\n".format(HOST, PORT))
    http_server = serve(app.wsgi_app, host=HOST, port=PORT, url_scheme=URL_SCHEME)